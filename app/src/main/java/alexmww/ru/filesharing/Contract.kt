package alexmww.ru.filesharing

interface Contract {
    interface Presenter {
        interface View {
            fun reconnect()
        }

        interface Model {
            fun firstConnectionFailed()
            fun firstConnectionSuccess()
            fun notifyDisconnect()
            fun updateList(new_list: List<String>)
        }
    }

    interface View {
        fun updateText(new_text: String)
        fun disconnect()
        fun start()
        fun fail()
    }
}