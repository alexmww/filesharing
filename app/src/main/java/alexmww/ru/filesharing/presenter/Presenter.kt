package alexmww.ru.filesharing.presenter

import alexmww.ru.filesharing.Contract
import alexmww.ru.filesharing.model.net.updaters.Updater

class Presenter(private val view: Contract.View) : Contract.Presenter.Model, Contract.Presenter.View {

    private val model = Updater(this)
    private val thread = Thread(model).apply { start() }

    //region Presenter.View
    override fun reconnect() {
        thread.start()
    }
    //endregion

    //region Presenter.Model
    override fun notifyDisconnect() {
        view.disconnect()
    }

    override fun firstConnectionSuccess() {
        view.start()
    }

    override fun firstConnectionFailed() {
        view.fail()
    }

    override fun updateList(new_list: List<String>) {
        val sb = StringBuilder()
        new_list.forEach { sb.append(it) }

        view.updateText(sb.toString())
    }
    //endregion
}