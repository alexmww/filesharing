interface SharedConsts {
    companion object {
        const val DEFAULT_IP = "224.0.0.224"
        const val SEPARATOR = "-"
        const val DEFAULT_SEND_DELAY = 1000
        const val DEFAULT_PORT = 2244
        const val BUFFER_SIZE = 1024
        const val MAX_TIMEOUT = 3
        const val MAX_WAITING_TIME = 2000 // ms
    }
}