package alexmww.ru.filesharing.view

import alexmww.ru.filesharing.Contract
import alexmww.ru.filesharing.R
import alexmww.ru.filesharing.presenter.Presenter
import android.graphics.drawable.Animatable
import android.graphics.drawable.Animatable2
import android.graphics.drawable.Drawable
import android.opengl.Visibility
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.graphics.drawable.Animatable2Compat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.WindowManager
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.load_screen.*


class MainActivity : AppCompatActivity(), Contract.View {
    private val presenter: Contract.Presenter.View = Presenter(this)
    //private val anim by lazy { (MyAnimator(loadingAnim)) }

    override fun onCreate(savedInstanceState: Bundle?) {
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setLoadingScreenContent()
    }

    private fun setLoadingScreenContent(){
        setContentView(R.layout.load_screen)
        supportActionBar?.hide()
    }

    private fun setMainContent(){
        setContentView(R.layout.activity_main)
        supportActionBar?.show()
    }

    private fun reconnect(){
        setLoadingScreenContent()
        presenter.reconnect()
    }

    //region Contract.View
    override fun updateText(new_text: String) {
        runOnUiThread {
            userList.text = new_text
        }
    }

    override fun start() {
        runOnUiThread {
            setMainContent()
        }
    }

    override fun fail() {
        runOnUiThread {
            Snackbar.make(logo, R.string.connection_lost, Snackbar.LENGTH_INDEFINITE).apply {
                view.setBackgroundColor(ContextCompat.getColor(this@MainActivity, R.color.colorPrimary))
                setAction(R.string.reconnect) { reconnect() }
                show()
            }
        }
    }

    override fun disconnect() {
        runOnUiThread {
            Snackbar.make(userList, R.string.connection_lost, Snackbar.LENGTH_INDEFINITE).apply {
                view.setBackgroundColor(ContextCompat.getColor(this@MainActivity, R.color.colorPrimary))
                setAction(R.string.reconnect) { reconnect() }
                show()
            }
        }
    }
    //endregion

    //region MyAnimator
    private class MyAnimator(val img: ImageView) {
        val anim = img.drawable as Animatable
        val isCompat = anim is Animatable2Compat
        val animCallBack = object : Animatable2.AnimationCallback() {
            override fun onAnimationEnd(drawable: Drawable?) {
                body()
            }
        }
        val animCallBackCompat = object : Animatable2Compat.AnimationCallback() {
            override fun onAnimationEnd(drawable: Drawable?) {
                body()
            }
        }

        private fun body() {
            img.post { anim.start() }
            Log.d("animtest", "animEnd")
        }

        fun start() {
            if (isCompat) (anim as Animatable2Compat).registerAnimationCallback(animCallBackCompat)
            else (anim as Animatable2).registerAnimationCallback(animCallBack)
            anim.start()
            img.visibility = VISIBLE
        }

        fun stop() {
            if (isCompat) (anim as Animatable2Compat).unregisterAnimationCallback(animCallBackCompat)
            else (anim as Animatable2).unregisterAnimationCallback(animCallBack)
            anim.stop()
            img.visibility = INVISIBLE
        }
    }
    //endregion
}
