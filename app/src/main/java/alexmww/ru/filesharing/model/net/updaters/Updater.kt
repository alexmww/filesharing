package alexmww.ru.filesharing.model.net.updaters

import SharedConsts
import alexmww.ru.filesharing.presenter.Presenter
import android.util.Log
import org.jetbrains.annotations.Contract
import java.net.DatagramPacket
import java.net.InetAddress
import java.net.MulticastSocket

class Updater(private val presenter: alexmww.ru.filesharing.Contract.Presenter.Model) : Runnable {

    private val addr = InetAddress.getByName(SharedConsts.DEFAULT_IP)
    private val handler: MessageHandler = MessageHandler(SharedConsts.MAX_TIMEOUT) { presenter.updateList(it) }
    private val sender: Sender = Sender(addr, SharedConsts.DEFAULT_PORT)
    private var socket: MulticastSocket? = null


    override fun run() {
        val buf = ByteArray(SharedConsts.BUFFER_SIZE)
        val packet = DatagramPacket(buf, buf.size)

        handler.clear()
        sender.reconnect()
        val senderThread = Thread(sender)

        try {
            socket = MulticastSocket(SharedConsts.DEFAULT_PORT).apply { joinGroup(addr) }
            socket?.soTimeout = SharedConsts.MAX_WAITING_TIME
        } catch (e: Exception) {
            Log.d("mytest1", "here")
            presenter.firstConnectionFailed()
            return
        }
        presenter.firstConnectionSuccess() //first connection is established

        try {
            senderThread.start()
            socket.use {
                while (true) {
                    socket?.receive(packet)
                    val msg = getMessage(packet)
                    Log.d("mytest", msg)
                    handler.handle(msg)
                    packet.length = buf.size
                }
            }
        } catch (e: Exception) {
            senderThread.interrupt()
            presenter.notifyDisconnect()
            return
        }

    }

    private fun getMessage(p: DatagramPacket): String {
        with(p) {
            return "$address${SharedConsts.SEPARATOR}$data"
        }
    }
}