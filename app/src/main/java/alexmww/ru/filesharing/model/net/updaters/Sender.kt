package alexmww.ru.filesharing.model.net.updaters

import SharedConsts
import java.lang.Exception
import java.lang.Thread.sleep
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.util.*

class Sender(ip: InetAddress, port: Int) : Runnable {

    private val id: String = UUID.randomUUID().toString()
    private var socket: DatagramSocket = DatagramSocket()
    private val packet: DatagramPacket = DatagramPacket(id.toByteArray(), id.length, ip, port)

    /**
     * Make sure you've invoked reconnect() before start Thread second time
     */
    override fun run() {
        try {
            socket.use {
                while (Thread.currentThread().isAlive) {
                    socket.send(packet)
                    sleep(SharedConsts.DEFAULT_SEND_DELAY.toLong())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun reconnect() {
        socket = DatagramSocket()
    }
}