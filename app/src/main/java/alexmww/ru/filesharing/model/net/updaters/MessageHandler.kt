package alexmww.ru.filesharing.model.net.updaters

import android.util.Log
import java.util.concurrent.ConcurrentHashMap

class MessageHandler(maxTimeout: Int, private val updater: (List<String>) -> Unit) {

    private var currentSize: Int = 0
    private val ipMap: IpMap = IpMap(maxTimeout)


    fun handle(message: String) {
        updateCounter(ipMap.setToZeroOrAdd(message))
    }

    fun updateCounter(newCounter: Int) {
        if (newCounter != currentSize) {
            currentSize = newCounter
            Log.d("mytest", ipMap.allIPs.toString())
            updater(ipMap.allIPs)
        }
    }

    fun clear(){
        ipMap.clear()
    }

    private class IpMap(private val maxCount: Int) : ConcurrentHashMap<String, Int>() {

        private val count: Int
            get() = this.size
        val allIPs: List<String>
            get() = this.keys.map { formatAsIp(it) }

        /**
         * Increases all counters in map
         * In case counter became equal or bigger than maxCount deletes this row
         *
         * @return current number of IPs
         */
        fun incCheckDelete(): Int {
            forEach { (k, v) -> if (v >= maxCount) remove(k) else this[k] = v + 1 }
            return count
        }

        /**
         * @param s unique ID - a string in format "IP-ID"
         * @return current number of IPs
         */
        fun setToZeroOrAdd(s: String): Int {
            this[s] = 0
            return count
        }

        private fun formatAsIp(s: String): String {
            return s.split(SharedConsts.SEPARATOR.toRegex())[0] // cut all the rest but IP
        }
    }
}